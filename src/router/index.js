import Vue from 'vue'
import Router from 'vue-router'
import appCollection from '@/components/appCollection'
import app1 from '@/components/app1'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'appCollection',
      component: appCollection
    },
    {
      path: '/app1',
      name: 'app1',
      component: app1
    }
  ]
})
